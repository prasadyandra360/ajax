﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="ajaxDropDownList.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/jquery-3.4.1.slim.min.js"></script>
    <script type="text/javascript">
        $(document).ready(
            function () {
                $("#Button1").click(
                    function (){
                        var obj = {};
                        obj.fname = $("#TxtStuFirstName").val();
                        obj.lname = $("#TxtStuLastName").val();
                        obj.mobileno = $("#TxtStuMObno").val();
                        obj.Address = $("#TxtStuAddress").val();
                        $.ajax(
                            {
                                type: "POST",
                                url: "WebForm2.aspx/insertStudent",
                                data: JSON.stringify(obj),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response)
                                {
                                    alert("success");
                                }
                            });
                    });
            });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <center>
               
                    <h1>Application Registration Of New User</h1>
                    <table>
                        <tr><td><strong>Enter FirstName</strong></td><td>
                            <asp:TextBox ID="TxtStuFirstName" runat="server"></asp:TextBox></td></tr>

                        <tr><td><strong>Enter LastName</strong></td><td>
                            <asp:TextBox ID="TxtStuLastName" runat="server"></asp:TextBox></td></tr>

                        <tr><td><strong>Enter DOB</strong></td><td>
                            <asp:TextBox ID="TxtStuDob" runat="server"></asp:TextBox></td></tr>

                        <tr><td><strong>Enter MobileNumber</strong></td><td>
                            <asp:TextBox ID="TxtStuMObno" runat="server"></asp:TextBox></td></tr>

                        <tr><td><strong>Select State</strong></td><td>
                            <asp:DropDownList ID="DropDownList1" runat="server" Width="130"></asp:DropDownList></td></tr>
                        
                        <tr><td><strong>Select city</strong></td><td>
                            <asp:DropDownList ID="DropDownList2" runat="server" Width="130"></asp:DropDownList></td></tr>
                        
                        <tr><td><strong>Select Location</strong></td><td>
                            <asp:DropDownList ID="DropDownList3" runat="server" Width="130"></asp:DropDownList></td></tr>
                        
                        <tr><td><strong>Select Collage</strong></td><td>
                            <asp:DropDownList ID="DropDownList4" runat="server" Width="130"></asp:DropDownList></td></tr>
                        <tr><td><strong>Enter Perment Address</strong></td><td>
                            <asp:TextBox ID="TxtStuAddress" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr>
                        <tr><td></td><td>
                            <asp:CheckBox ID="CheckBox1" runat="server" />If present and perminant address are same than click here</td></tr>
                           <tr><td><strong>Enter Present Address</strong></td><td>
                            <asp:TextBox ID="TextBox6" runat="server" TextMode="MultiLine"></asp:TextBox></td></tr>
                        <tr><td colspan="2"><Center>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Button1" runat="server" Text="Submit" /> &nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp   <asp:Button ID="Button2" runat="server" Text="Reset" />
                            <br />
                            <br />
                            <asp:Label ID="Label1" runat="server"></asp:Label>
                            </Center></td></tr>
                    </table>
               
            </center>
        </div>
    </form>
</body>
</html>
